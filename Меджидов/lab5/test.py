import unittest
import lab5


class TestLab5(unittest.TestCase):
    def test_division(self):
        self.assertEqual(lab5.division(2, 1), 2)
        self.assertEqual(lab5.division(1, 2), 0.5)
        self.assertEqual(lab5.division(2, 0), None)
        self.assertEqual(lab5.division('a', 1), None)
        self.assertEqual(lab5.division('1', 1), 1)

    def test_add(self):
        self.assertEqual(lab5.add(2, 1), 3)
        self.assertEqual(lab5.add(1, 2), 3)
        self.assertEqual(lab5.add(2, 0), 2)
        self.assertEqual(lab5.add(2.1, 1.2), 3.3)
        self.assertEqual(lab5.add('a', 1), 'a1')
        self.assertEqual(lab5.add('1', 1), '11')

    def test_is_neg(self):
        self.assertRaises(lab5.NegNumError, lab5.is_neg, -1)
        self.assertRaises(ValueError, lab5.is_neg, 'a')
        self.assertRaises(lab5.NotIntError, lab5.is_neg, 0.1)
        self.assertEqual(lab5.is_neg(1), 1)
        self.assertEqual(lab5.is_neg(0), 0)


if __name__ == '__main__':
    unittest.main()
